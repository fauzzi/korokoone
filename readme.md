# Korokoone pakai Laravel

Kirim email ke Admin jika ada filed yang tidak di isi oleh visitor
1. Untuk simulasi bisa menggunakan mailtrap.io dan kedepannya dapat menggunakan akun email Admin yang sebenarnya
2. Clone atau download via zip file, go to directory checkout dan run project 'php artisan serve'
3. Untuk database sudah di setup menggunakan sqlite
4. Untuk melihat hasil simulasi kiriman email silahkan setup berikut:
  - Silahkan registrasi di https://mailtrap.io/
  - Ubah .env :
    MAIL_DRIVER=smtp
	MAIL_HOST=mailtrap.io
	MAIL_PORT=2525
	MAIL_USERNAME=username anda di mailtrap.io (Ubah)
	MAIL_PASSWORD=password anda di mailtrap.io (Ubah)
	MAIL_ENCRYPTION=null

- Ubah di config/mail.php
  'from' => ['address' => "alamat email anda (Ubah)", 'name' => "nama anda (Ubah)"],
  

